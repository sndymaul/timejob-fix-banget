import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { KatrestoranPage } from './katrestoran.page';

describe('KatrestoranPage', () => {
  let component: KatrestoranPage;
  let fixture: ComponentFixture<KatrestoranPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KatrestoranPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(KatrestoranPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
