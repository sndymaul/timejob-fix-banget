import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IsipopulerPage } from './isipopuler.page';

describe('IsipopulerPage', () => {
  let component: IsipopulerPage;
  let fixture: ComponentFixture<IsipopulerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IsipopulerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IsipopulerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
