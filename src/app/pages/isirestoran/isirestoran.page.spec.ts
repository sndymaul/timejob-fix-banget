import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IsirestoranPage } from './isirestoran.page';

describe('IsirestoranPage', () => {
  let component: IsirestoranPage;
  let fixture: ComponentFixture<IsirestoranPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IsirestoranPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IsirestoranPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
