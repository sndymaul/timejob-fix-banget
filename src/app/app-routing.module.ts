import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
{
    path: '',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'isipopuler',
    loadChildren: () => import('./pages/isipopuler/isipopuler.module').then( m => m.IsipopulerPageModule)
  },
  {
    path: 'katperusahaan',
    loadChildren: () => import('./pages/katperusahaan/katperusahaan.module').then( m => m.KatperusahaanPageModule)
  },
  {
    path: 'kattoko',
    loadChildren: () => import('./pages/kattoko/kattoko.module').then( m => m.KattokoPageModule)
  },
  {
    path: 'katrestoran',
    loadChildren: () => import('./pages/katrestoran/katrestoran.module').then( m => m.KatrestoranPageModule)
  },
  {
    path: 'isitoko',
    loadChildren: () => import('./pages/isitoko/isitoko.module').then( m => m.IsitokoPageModule)
  },
  {
    path: 'isirestoran',
    loadChildren: () => import('./pages/isirestoran/isirestoran.module').then( m => m.IsirestoranPageModule)
  },
  {
    path: 'isiperusahaan',
    loadChildren: () => import('./pages/isiperusahaan/isiperusahaan.module').then( m => m.IsiperusahaanPageModule)
  },
  
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'create-profile',
    loadChildren: () => import('./create-profile/create-profile.module').then( m => m.CreateProfilePageModule)
  },
  {
    path: 'edit-profile',
    loadChildren: () => import('./edit-profile/edit-profile.module').then( m => m.EditProfilePageModule)
  },
  {
    path: 'deskripsi-inbox',
    loadChildren: () => import('./deskripsi-inbox/deskripsi-inbox.module').then( m => m.DeskripsiInboxPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
