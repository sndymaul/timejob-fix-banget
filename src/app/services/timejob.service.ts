import { Injectable } from '@angular/core';
import { AngularFirestore,AngularFirestoreDocument,AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimejobService {

perusahaan: Observable<any[]>;

  constructor(private afs: AngularFirestore) { }

  getPrusahaan()
  {
  this.perusahaan = this.afs.collection('perusahaan').valueChanges();
  }

}
