import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'tab1',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tab1/tab1.module').then(m => m.Tab1PageModule)
          }
        ]
      },
      {
        path: 'tab2',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tab2/tab2.module').then(m => m.Tab2PageModule)
          }
        ]
      },
      {
        path: 'tab3',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tab3/tab3.module').then(m => m.Tab3PageModule)
          }
        ]
      },
      {
        path: 'katperusahaan',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pages/katperusahaan/katperusahaan.module').then(m => m.KatperusahaanPageModule)
          }
        ]
      },
      {
        path: 'katrestoran',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pages/katrestoran/katrestoran.module').then(m => m.KatrestoranPageModule)
          }
        ]
      },
      {
        path: 'kattoko',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pages/kattoko/kattoko.module').then(m => m.KattokoPageModule)
          }
        ]
      },
      
      {
        path: '',
        redirectTo: '/tabs/tab3',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab3',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
