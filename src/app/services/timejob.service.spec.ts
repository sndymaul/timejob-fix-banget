import { TestBed } from '@angular/core/testing';

import { TimejobService } from './timejob.service';

describe('TimejobService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TimejobService = TestBed.get(TimejobService);
    expect(service).toBeTruthy();
  });
});
