import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KattokoPage } from './kattoko.page';

const routes: Routes = [
  {
    path: '',
    component: KattokoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KattokoPageRoutingModule {}
